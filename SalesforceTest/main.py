# Copyright (C) 2020 Alteryx, Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Example input tool."""
from ayx_plugin_sdk.core import (
    FieldType,
    InputConnectionBase,
    Metadata,
    Plugin,
    ProviderBase,
    RecordPacket,
    register_plugin,
)
from ayx_plugin_sdk.core.exceptions import WorkflowRuntimeError


class SalesforceTest(Plugin):
    """Concrete implementation of an AyxPlugin."""

    def __init__(self, provider: ProviderBase) -> None:
        """Construct a plugin."""
        self.provider = provider
        self.tool_config = provider.tool_config
        self.config_value = self.tool_config["Value"]
        self.output_anchor = self.provider.get_output_anchor("Output")

        self.output_metadata = Metadata()

        if float(self.config_value) > 0.5:
            raise WorkflowRuntimeError(
                "Values greater than 0.5 are not allowed.")

        self.provider.io.info("Plugin initialized.")

    def on_input_connection_opened(self, input_connection: InputConnectionBase) -> None:
        """Initialize the Input Connections of this plugin."""
        raise NotImplementedError("Input tools don't have input connections.")

    def on_record_packet(self, input_connection: InputConnectionBase) -> None:
        """Handle the record packet received through the input connection."""
        raise NotImplementedError("Input tools don't receive packets.")

    def on_complete(self) -> None:
        """Create all records."""
        import pandas as pd
        from platforms import Platform

        salesforce = Platform(platform_name='Salesforce',
                              client_id='3MVG9n_HvETGhr3BB5BzPeDmKAXhpGJB8mwhvzV9pDZKaHLh5KsaVQ2ESDTLv.ZNuZ0QYNZbLIz2X1LpFgCu6',
                              client_secret='4429A9F9C57FA6F88AD6572D11856424A31C978EF6967011309DCE0C5B000D6A',
                              username='krish@221b.com',
                              password='potterforce1')

        query = '''SELECT Name, Email, Title FROM Contact ORDER BY Name DESC'''
        result = salesforce.get_data(
            '/services/data/v39.0/query/', query)

        # service_now = Platform(platform_name='Servicenow',
        #                        client_id="ac0dd3408c1031006907010c2cc6ef6d",
        #                        client_secret="m13h1jzco6nhdin71vos",
        #                        username="admin",
        #                        password="Zvdj3y5e8AFAndH",
        #                        )

        # service_now.authenticate()
        # url = "https://dev82371.service-now.com/api/now/table/incident?sysparm_display_value=true&sysparm_exclude_reference_link=true&sysparm_limit=10"
        # result = service_now.get_data(url)

        for key in result:
            self.output_metadata.add_field(key, FieldType.v_string, size=100)

        self.output_anchor.open(self.output_metadata)

        data_frame = pd.DataFrame(result)

        packet = RecordPacket.from_dataframe(self.output_metadata, data_frame)

        self.output_anchor.write(packet)
        self.provider.io.info("Completed processing records.")


AyxPlugin = register_plugin(SalesforceTest)
